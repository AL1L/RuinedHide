package me.diamonddev.ruined.hide;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.diamonddev.ruined.integration.RuinedWorldBorder;

public class Plugin extends JavaPlugin {

	@Override
	public void onEnable() {
		getConfig().getDefaults();
		saveDefaultConfig();

		PluginManager manager = getServer().getPluginManager();
		manager.registerEvents(new CommandListener(), this);
	}

	public static Plugin gi() {
		return (Plugin) Bukkit.getPluginManager().getPlugin("RuinedHide");

	}

	@Override
	public void onDisable() {
	}
}
