package me.diamonddev.ruined.hide;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.TabCompleteEvent;

public class CommandListener implements Listener {

	String[] bukkitCommands = { "version", "plugins", "reload", "timings", "achievement", "ban", "ban-ip", "banlist",
			"clear", "debug", "defaultgamemode", "deop", "difficulty", "effect", "gamemode", "gamerule", "give", "help",
			"?", "kick", "kill", "list", "me", "op", "pardon", "pardon-ip", "playsound", "save-all", "save-on",
			"save-off", "say", "scoreboard", "seed", "setblock", "fill", "setidletimeout", "setworldspawn",
			"spawnpoint", "spawnpoint", "stop", "summon", "tell", "tellraw", "testfor", "testforblock", "time",
			"toggledownfall", "tp", "weather", "worldborder", "whitelist", "xp" };

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommandSent(PlayerCommandPreprocessEvent e) {
		Player player = e.getPlayer();
		String message = e.getMessage().toLowerCase().substring(1, e.getMessage().length());
		//String[] messageWords = message.split(" ");
		List<String> blockedCMDs = Plugin.gi().getConfig().getStringList("Options.BlockedCommands");
		List<String> blockedPhrases = Plugin.gi().getConfig().getStringList("Options.BlockedPhrases");
		//PluginCommand command = Bukkit.getServer().getPluginCommand(messageWords[0]);

		/*
		 * List<String> bukkitCommandsList = Arrays.asList(bukkitCommands); if
		 * ((command == null || !command.isRegistered()) &&
		 * !bukkitCommandsList.contains(messageWords[0])) { return; }
		 */
		// Blocked Phrases
		for (String phrase : blockedPhrases) {
			phrase = phrase.toLowerCase();
			if (message.contains(phrase)) {

				if (!player.isOp()) {
					player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "WARNING!" + ChatColor.RED
							+ " This command contains the blocked phrase \"" + phrase + "\"");
					e.setCancelled(true);
					return;
				} else {
					player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "WARNING!" + ChatColor.RED
							+ " This command contains the blocked phrase \"" + phrase
							+ "\" and because you are op you are bypassing this block.");
				}
			}
		}

		// Blocked Commands
		for (String cmd : blockedCMDs) {
			cmd = cmd.toLowerCase();
			if (message.startsWith(cmd)) {
				if (!player.isOp()) {
					e.setMessage("unknownCommand123");
					return;
				} else {
					player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "WARNING!" + ChatColor.RED
							+ " This is a blocked command and because you are op you are bypassing this block.");
				}
			}
		}

		// Bukkit Hidden Syntax
		if (message.contains(":") && message.length() >= 3) {
			if (!player.isOp()) {
				e.setMessage("unknownCommand123");
				return;
			} else {
				player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "WARNING!" + ChatColor.RED
						+ " This is a blocked command and because you are op you are bypassing this block.");
				return;
			}
		}
	}

	// Tab pressed while entering command
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommandTabPressed(TabCompleteEvent e) {
		if (!(e.getSender() instanceof Player))
			return;
		Collection<String> col = e.getCompletions();
		List<String> returns = new ArrayList<String>();
		List<String> returnsToRemove = new ArrayList<String>();
		returns.addAll(col);
		Player player = (Player) e.getSender();
		if (!player.isOp()) {
			for (String item : returns) {
				if (item.length() >= 3) {
					if (item.contains(":")) {
						returnsToRemove.add(item);
					}
				}
			}
		}
		returns.removeAll(returnsToRemove);
		e.setCompletions(returns);
	}

	public void test() {

	}
}
